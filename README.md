# spinr

spinr is the easiest way to create Twitter threads
It was created by [Stéphane Labruyère](https://www.slabruyere.net) and is written in Clojure and ClojureScript.
The app can be found at [this address](https://www.slabruyere.net/apps/spinr).

## Development mode

To start the Figwheel compiler, navigate to the project folder and run the following command in the terminal:

```
lein figwheel
```

Figwheel will automatically push cljs changes to the browser. The server will be available at [http://localhost:3449](http://localhost:3449) once Figwheel starts up. 

Figwheel also starts `nREPL` using the value of the `:nrepl-port` in the `:figwheel`
config found in `project.clj`. By default the port is set to `7002`.

The figwheel server can have unexpected behaviors in some situations such as when using
websockets. In this case it's recommended to run a standalone instance of a web server as follows:

```
lein do clean, run
```

The application will now be available at [http://localhost:3000](http://localhost:3000).


### Optional development tools

Start the browser REPL:

```
$ lein repl
```
The Jetty server can be started by running:

```clojure
(start-server)
```
and stopped by running:
```clojure
(stop-server)
```

## Running the tests

### Clojure
To run the Clojure tests, simply run
```
lein test
```

### ClojureScript
To run [cljs.test](https://github.com/clojure/clojurescript/blob/master/src/main/cljs/cljs/test.cljs) tests, please use

```
lein doo
```

## Building for release

```
lein do clean, uberjar
```

## License
Copyright © 2019 Stéphane Labruyère
Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
