(ns spinr.config
  (:require [environ.core :refer [env]]))

(def config
  {:consumer-key    (env :consumer-key "key")
   :consumer-secret (env :consumer-secret "secret")
   :callback-uri    (env :callback-uri "http://localhost:8080")})
