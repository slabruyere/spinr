(ns spinr.handlers
  (:require [cheshire.core :refer [generate-string
                                   parse-string]]
            [spinr.oauth :refer [get-request-token
                                 get-user-approval-uri
                                 get-access-token
                                 get-user-profile
                                 post-tweet]]
            [spinr.util :as u]))

(defn get-request-token-handler
  "1. Get request token from Twitter
   2. Get user approval URI
   3. Return request-token (for cookies) and approval URI for redirect"
  [{{accept-cookies :accept-cookies} :body}]
  (if accept-cookies
    (try
      (let [request-token     (get-request-token)
            user-approval-uri (get-user-approval-uri request-token)]
        {:status  200
         :cookies {"request-token" {:value   (generate-string request-token)
                                    :max-age 86400
                                    :secure  true}}
         :headers {"Content-Type" "application/json"}
         :body    {:user-approval-uri user-approval-uri}})
      (catch Exception e
        {:status  500
         :headers {"Content-Type" "application/json"}
         :body    {:err (str "Server error " (.getMessage e))}}))
    {:status  500
     :headers {"Content-Type" "application/json"}
     :body    {:message "You need to accept cookies tu use spinr."}}))

(defn get-cookie
  [cookies cookie-name]
  (-> cookies
      (get-in [cookie-name :value])
      (parse-string true)))

(defn get-profile-handler
  "1. Retrieve request-token from cookies and verifier from request body
   2. Get access-token from Twitter (in which we get screen_name) OR from cookies
   3. Get full user profile
   4. Return light profile and set access-token cookie"
  [{{verifier :verifier} :body cookies :cookies}]
  (let [request-token (get-cookie cookies "request-token")
        access-token  (if-not verifier
                        (get-cookie cookies "access-token")
                        (get-access-token request-token verifier))
        screen-name   (:screen_name access-token)
        user-profile  (get-user-profile access-token
                                        {:screen_name screen-name})]
    {:status  200
     :cookies {"access-token" (generate-string access-token)}
     :headers {"Content-Type" "application/json"}     
     :body    {:name            (:name user-profile)
               :handle          (str "@" screen-name)
               :profile-picture (:profile_image_url_https user-profile)}}))

(defn post-tweets-handler
  "1. Retrieve access-token from cookies
   2. Create tweets from text and options
   3. Loop over the tweets keeping the previous tweet id to make thread
   4. If all went well, return a status 204"
  [{{:keys [text options]} :body cookies :cookies}]
  (try
    (let [access-token (get-cookie cookies "access-token")]
      (loop [tweets      (u/get-thread text options)
             in-reply-to nil]
        (if-not (empty? tweets)
          (let [params   (merge {:status (first tweets)}
                                (when-not (nil? in-reply-to)
                                  {:in_reply_to_status_id in-reply-to}))
                {id :id} (post-tweet access-token params)]
            (recur (rest tweets) id))
          {:status 204})))
    (catch Exception e
      {:status  500
       :headers {"Content-Type" "application/json"}       
       :body    {:err (str "Server error " (.getMessage e))}})))
