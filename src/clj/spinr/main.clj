(ns spinr.main
  (:require [reitit.ring :as reitit-ring]
            [spinr.middleware :refer [middleware]]
            [hiccup.page :refer [include-js include-css html5]]
            [environ.core :refer [env]]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [ring.middleware.json :refer [wrap-json-response
                                          wrap-json-body]]
            [spinr.handlers :refer [get-request-token-handler
                                    get-profile-handler
                                    post-tweets-handler]]))

(def mount-target
  [:div#app
   [:h2 "Welcome to spinr"]
   [:p "Please wait while the app is loading..."]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name    "viewport"
           :content "width=device-width, initial-scale=1"}]
   [:title "spinr, the easiest way to create Twitter threads"]
   (include-css (if (env :prod) "/css/site.min.css" "/css/site.css"))])

(defn loading-page []
  (html5
   (head)
   [:body {:class "body-container"}
    mount-target
    (include-js "/js/app.js")]))

(defn index-handler
  [_request]
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    (loading-page)})

(defn make-handler
  [handler]
  (-> handler
      wrap-cookies
      (wrap-json-body {:keywords? true})
      wrap-json-response))

(def app
  (reitit-ring/ring-handler
   (reitit-ring/router
    [["/" {:get {:handler index-handler}}]
     ["/about" {:get {:handler index-handler}}]
     ["/get-request-token" {:post {:handler (make-handler get-request-token-handler)}}]
     ["/get-profile" {:post {:handler (make-handler get-profile-handler)}}]
     ["/post-tweets" {:post {:handler (make-handler post-tweets-handler)}}]])
   (reitit-ring/routes
    (reitit-ring/create-resource-handler {:path "/" :root "/public"})
    (reitit-ring/create-default-handler))
   {:middleware middleware}))
