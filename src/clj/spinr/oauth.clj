(ns spinr.oauth
  (:require [oauth.client :as oauth]
            [spinr.config :refer [config]]
            [cheshire.core :refer [parse-string]]
            [clj-http.client :as http]))

(def ^:private consumer
  (oauth/make-consumer (:consumer-key config)
                       (:consumer-secret config)
                       "https://api.twitter.com/oauth/request_token"
                       "https://api.twitter.com/oauth/access_token"
                       "https://api.twitter.com/oauth/authorize"
                       :hmac-sha1))

(defn get-request-token []
  (oauth/request-token consumer
                       (:callback-uri config)))


(defn get-user-approval-uri
  [request-token]
  (oauth/user-approval-uri consumer 
                           (:oauth_token request-token)))

(defn get-access-token
  "Provides screen_name needed for get-user-profile"
  [request-token verifier]
  (oauth/access-token consumer 
                      request-token
                      verifier))

(defn get-user-profile
  "profile_image_url_https"
  [access-token params]
  (let  [user-show-url "https://api.twitter.com/1.1/users/show.json"
         credentials   (oauth/credentials
                        consumer
                        (:oauth_token access-token)
                        (:oauth_token_secret access-token)
                        :GET
                        user-show-url                            
                        params)
         response      (http/get user-show-url {:query-params (merge credentials params)})]
    (parse-string (:body response) true)))

(defn post-tweet
  "in_reply_to_status_id id"
  [access-token params]
  (let [tweet-update-url "https://api.twitter.com/1.1/statuses/update.json"
        credentials      (oauth/credentials
                          consumer
                          (:oauth_token access-token)
                          (:oauth_token_secret access-token)
                          :POST
                          tweet-update-url
                          params)
        response         (http/post tweet-update-url {:query-params (merge credentials params)})]
    (parse-string (:body response) true)))
