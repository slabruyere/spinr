(ns spinr.server
  (:require
   [spinr.main :refer [app]]
   [environ.core :refer [env]]
   [ring.adapter.jetty :refer [run-jetty]])
  (:gen-class))

(def ^:private default-port 3000)

(defn -main [& args]
  (let [port (try
               (Integer/parseInt (env :port (str default-port)))
               (catch Exception e
                 default-port))]
    (run-jetty app {:port port :join? false})))
