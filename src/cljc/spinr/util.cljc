(ns spinr.util
  (:require [clojure.string :refer [split
                                    reverse
                                    replace
                                    join
                                    index-of]]))

(def MAX-TWEET-LENGTH 280)

(def MAX-TWEETS 25)

(def too-many-tweets
  (str "Cannot handle nor send more than " MAX-TWEETS " tweets."))

(defn safe-subs
  [s start end]
  (let [l (count s)]
    (if (> end l)
      (subs s start)
      (subs s start end))))

(defn tweet->hiccup
  "replace \n with <br> tags"
  [tweet]
  (interpose [:br]
             (split tweet #"\n")))

(def separators "\n\n")

(def thread-str "THREAD!")

(def k-over-n-str "kk/nn")

(defn get-tweet-max-length
  [i opts]
  (- MAX-TWEET-LENGTH
     (if (and (= 1 i) (:thread opts))
       (+ (count separators) (count thread-str))
       0)
     (if (:k-over-n opts)
       (+ (count separators) (count k-over-n-str))
       0)))

(defn get-split-idx
  [reversed-text from-index]
  (let [dot-idx   (index-of reversed-text " ." from-index)
        exl-idx   (index-of reversed-text " !" from-index)
        qst-idx   (index-of reversed-text " ?" from-index)
        spc-idx   (index-of reversed-text " " from-index)
        threshold (+ from-index (/ MAX-TWEET-LENGTH 10))]
    (if (or (and dot-idx (< dot-idx threshold))
            (and exl-idx (< exl-idx threshold))
            (and qst-idx (< qst-idx threshold)))
      (apply min (filter identity (list dot-idx exl-idx qst-idx)))
      (apply min (filter identity (list spc-idx dot-idx exl-idx qst-idx))))))

(defn get-tweet-text
  "Make sure idx < max-length"
  [text max-length]
  (let [reversed-text (reverse text)
        length        (count text)
        from-index    (- length max-length)
        split-idx     (if (> from-index 0)
                        (get-split-idx reversed-text from-index)
                        0)
        real-idx      (- length split-idx)]
    (safe-subs text 0 (min real-idx max-length))))

(defn add-options-to-tweet
  [tweet i opts]
  (str tweet
       (when (and (= 1 i) (:thread opts))
         (join [separators thread-str]))
       (when (:k-over-n opts)
         (join [separators (replace k-over-n-str #"^kk" (str i))]))))

(defn add-n
  [n tweet]
  (replace tweet #"nn$" n))

(defn get-tweets
  [text opts]
  (loop [i         1
         tweets    []
         text-left text]
    (if-not (empty? text-left)
      (let [max-length         (get-tweet-max-length i opts)
            tweet              (get-tweet-text text-left max-length)
            tweet-with-options (add-options-to-tweet tweet i opts)
            future-text-left   (subs text-left (count tweet))
            is-last-tweet?     (empty? future-text-left)]
        (recur (inc i)
               (conj tweets (if (and (= 1 i) is-last-tweet?)
                              tweet
                              tweet-with-options))
               future-text-left))
      tweets)))

(defn get-thread
  "- adjust tweet-length on options
   - for all n*tweet-length, find previous space and make sure tweet is not too long"
  [s opts]
  (let [tweets (get-tweets s opts)
        n      (count tweets)]
    (if (> n MAX-TWEETS)
      (throw #?(:clj  (Exception. too-many-tweets)
                :cljs (js/Error too-many-tweets)))
      (if (:k-over-n opts)
        (map (partial add-n (str n)) tweets)
        tweets))))

