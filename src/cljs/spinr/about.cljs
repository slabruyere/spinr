(ns spinr.about)

(defn about-page []
  (fn []
    [:span.main
     [:p "Threads are very popular on Twitter these days. But how difficult are they to create?"]
     [:p "spinr is the easiest way to create Twitter threads. Simply write or paste the whole text you want to post in the \"Thread content\" section and you're mostly done. Add options to the thread if you wish (like adding a \"k/n\" counter at the end of each tweet, or a big \"THREAD!\" to conclude the first tweet) and click \"Tweet\"!"]
     [:p "And just like that, your Thread is online!"]
     [:p "Note that spinr only use cookies to keep you logged in and will never keep your data or use it for any other purpose than display (like your name, Twitter handle and profile picture)."]
     [:p "spinr is written in "
      [:a {:href  "https://clojure.org/"
           :title "Clojure"}
       "Clojure"]
      " and "
      [:a {:href  "https://clojurescript.org/"
           :title "ClojureScript"}
       "ClojureScript"]
      ". It was created by "
      [:a {:href "https://www.twitter.com/_slabruyere"}
       "Stéphane Labruyère"]
      ". You can learn more by visiting "
      [:a {:href  "https://www.slabruyere.net/"
           :title "Stéphane Labruyère's blog"}
       "his blog"]
      "."]]))
