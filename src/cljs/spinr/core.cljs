(ns spinr.core
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [reitit.frontend :as reitit]
            [clerk.core :as clerk]
            [accountant.core :as accountant]
            [spinr.home :refer [home-page demo-page]]
            [spinr.about :refer [about-page]]
            [cemerick.url :as c]
            [clojure.walk :refer [keywordize-keys]]))

;; -------------------------
;; Routes

(defn ^:private match-route-with-query-params
  [router path]
  (let [query-params (->> (c/url path)
                          :query
                          keywordize-keys)]
    (-> (reitit/match-by-path router path)
        (assoc :query-params query-params))))


(def router
  (reitit/router
   [["/" :index]
    ["/about" :about]]))

(defn path-for [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))

;; -------------------------
;; Translate routes -> page components

(defn page-for [route]
  (case route
    :index #'home-page
    :about #'about-page))

;; -------------------------
;; Page mounting component

(defn current-page []
  (fn []
    (let [page (:current-page (session/get :route))]
      [:div
       [:div.top-menu
        [:h1
         [:a
          {:href  "/"
           :title "spinr, the easiest way to create Twitter threads"}
          "spinr"
          [:small "the easiest way to create Twitter threads"]]]
        [:div.menu-links
         [:p
          [:a {:href (path-for :index)} "home"]
          " | "
          [:a {:href (path-for :about)} "about"]
          " | "
          [:a
           {:href (str "https://twitter.com/intent/tweet"
                       "?url=" (js/encodeURIComponent "https://spinr.herokuapp.com/")
                       "&text="
                       (js/encodeURIComponent "@_spinr is the easiest way to create Twitter threads, try it !")
                       "&hashtags=spinr,thread,tweetstorm")}
           "tweet your ❤ for spinr!"]]]]
       [page]])))

;; -------------------------
;; Initialize app

(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (clerk/initialize!)
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (let [match        (match-route-with-query-params router path)    
            current-page (:name (:data match))
            route-params (:path-params match)
            query-params (:query-params match)]
        (reagent/after-render clerk/after-render!)
        (session/put! :route
                      {:current-page (page-for current-page)
                       :route-params route-params
                       :query-params query-params})
        (clerk/navigate-page! path)))
    :path-exists?
    (fn [path]
      (boolean (reitit/match-by-path router path)))})
  (accountant/dispatch-current!)
  (mount-root))
