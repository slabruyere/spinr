(ns spinr.home
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [reagent.cookies :as cookies]
            [spinr.util :as u]
            [cljs-time.core :as t]
            [cljs-time.format :as f]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]))

(def ^:private default-text
  "What's happening?")

(def ^:private default-picture
  "images/default_profile_400x400.png")

(def ^:private date
  (f/unparse (f/formatter "MMM d")
             (t/now)))


(defonce author
  (atom {:name            "John Doe"
         :handle          "@JohnDoe"
         :profile-picture default-picture}))

(defonce profile-loaded?
  (atom false))

(defn count-lines-needed
  [text]
  (reagent/track
   (fn []
     (+  (int (/ (count @text) 80))
         (count (re-seq #"\n" @text))))))

(defn do-tweet
  [text options demo?]
  (if demo?
    (do
      (reset! text default-text)
      (js/alert "Nothing happened!"))
    (go
      (let [response (<! (http/post "/post-tweets"
                                    {:json-params {:text    @text
                                                   :options @options}}))]
        (if (= (:status response) 204)
          (do
            (reset! text default-text)
            (js/alert "Your thread is online !"))
          (js/alert "Something wrong happened."))))))

(defn load-profile-picture
  [profile-picture]
  (let [image (js/Image.)]
    (set! (.-onload image)
          (fn []
            (swap! author assoc :profile-picture profile-picture)))
    (set! (.-src image) profile-picture)))

(defn load-profile
  [query-params author]
  (when-not @profile-loaded?
    (go
      (let [verifier        (or (cookies/get-raw "verifier")
                                (:oauth_verifier query-params))
            params          {:verifier verifier}
            profile         (<! (http/post "/get-profile"
                                           {:json-params params}))
            profile-picture (get-in profile [:body :profile-picture])]
        (reset! author (-> (:body profile)
                           (assoc :profile-picture default-picture)))
        (load-profile-picture profile-picture)
        (reset! profile-loaded? true)))))

(defn make-controls
  [options]
  [:div.controls
   [:label [:input
            {:type      "checkbox"
             :checked   (:thread @options)
             :on-change #(swap! options update :thread not)}]
    "Append \"THREAD!\" to 1st tweet"]
   [:label [:input
            {:type      "checkbox"
             :checked   (:k-over-n @options)
             :on-change #(swap! options update :k-over-n not)}]
    "Append k/n to tweets"]])

(defn make-tweet
  [author idx tweet]
  [:div.tweet
   {:key (str "tweet-" idx)}
   [:div.left-column
    [:div.avatar
     [:img {:src (:profile-picture @author)}]]
    [:div.bottom-bar]]
   [:div.main
    [:div.header
     [:div.author-name (:name @author)]
     [:div.author-handle (:handle @author)]
     [:div.date-separator "."]
     [:div.date date]]
    [:div.body
     (u/tweet->hiccup tweet)]]])

(defn make-spinr
  [query-params demo?]
  (reagent/with-let
    [text (atom default-text)
     options (atom {:k-over-n true
                    :thread   true})
     rows (count-lines-needed text)]
    (when-not demo?
      (load-profile query-params author))
    [:<>
     [:div.text
      [:h2 "Thread content"
       (when demo?
         [:small " (demo)"])]
      [:div.controls-container
       (make-controls options)]
      [:textarea
       {:value     @text
        :cols      40
        :rows      (+ 5 @rows)
        :on-change #(reset! text (.. % -target -value))}]]
     [:div.tweets
      [:div
       [:h2 "Preview"
        (when demo?
          [:small " (demo)"])]
       [:button
        {:on-click #(do-tweet text options demo?)}
        "Tweet"]]
      [:div.separator]
      [:div.thread
       (doall
        (map-indexed (partial make-tweet author)
                     (u/get-thread @text @options)))]]]))

(defn authorize-spinr
  [accept-cookies?]
  (when @accept-cookies?
    (go
      (let [response (<! (http/post "/get-request-token"
                                    {:json-params
                                     {:accept-cookies @accept-cookies?}}))            
            uri      (get-in response [:body :user-approval-uri])]
        (when uri
          (set! (.-location js/window) uri))))))

(defn make-authorization
  []
  (reagent/with-let
    [accept-cookies? (atom true)]
    [:<>
     [:div.authorization-main
      [:small
       {:title    "Close help message and use demo"
        :on-click #(let [elem (aget
                               (.getElementsByClassName
                                js/document
                                "authorization-main")
                               0)]
                     (.removeChild (aget elem "parentNode") elem))}
       "x"]
      [:h2 "Authorization needed"]
      [:div.authorization
       [:p "We need your permission to post your threads. We do not use or keep your data. The use of cookies is only to keep you logged in. Click the button to be redirected to a Twitter authorization page."]
       [:label [:input
                {:type      "checkbox"
                 :checked   @accept-cookies?
                 :on-change #(swap! accept-cookies? not)}]
        "Authorize the use of cookies"]
       [:div.authorization-button
        [:button
         {:disabled (not @accept-cookies?)
          :on-click #(authorize-spinr accept-cookies?)}
         "Authorize app on Twitter"]]]]
     (make-spinr {} true)]))

(defn create-home-page
  [demo?]
  (reagent/with-let
    [query-params (session/get-in [:route :query-params])
     back-from-twitter-auth-page? (and (:oauth_verifier query-params)
                                       (:oauth_token query-params))
     _ (when (and back-from-twitter-auth-page?
                  (cookies/contains-key? "request-token")
                  (not (cookies/contains-key? "verifier")))
         (cookies/set! "verifier"
                       (:oauth_verifier query-params)
                       {:raw?    true
                        :secure? true
                        :max-age (* 24 3600)}))
     logged-in? (or
                 back-from-twitter-auth-page?
                 (cookies/contains-key? "access-token")
                 (and (cookies/contains-key? "request-token")
                      (cookies/contains-key? "verifier")))]
    (fn []
      [:div.main
       [:div.container
        (if (or demo? logged-in?)
          (make-spinr query-params demo?)
          (make-authorization))]])))

(defn home-page []
  (create-home-page false))

(defn demo-page []
  (create-home-page true))
