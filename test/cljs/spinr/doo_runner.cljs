(ns spinr.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [spinr.core-test]))

(doo-tests 'spinr.core-test)
